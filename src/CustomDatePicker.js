import React, { Component } from 'react'

import DayPickerInput from './react-day-picker/DayPickerInput'
import './react-day-picker/lib/style.css'

import { formatDate, parseDate } from './react-day-picker/moment'

import styled from 'styled-components'
const InputFromTo = styled.div`

  .DayPicker-Day {
    border-radius: 0 !important;
  }
  .DayPicker-Day--start {
    border-top-left-radius: 50% !important;
    border-bottom-left-radius: 50% !important;
  }
  .DayPicker-Day--end {
    border-top-right-radius: 50% !important;
    border-bottom-right-radius: 50% !important;
  }
  .DayPickerInput > input{
   
    border-radius: 5px;
    border: 1px solid #eae3ea;
    width: 150px;
    height: 30px;
    text-align: center;
    margin: 5px;
    cursor: pointer;
    color: #989698;
  }
  .DayPickerInput-Overlay {
    width: 600px;
    right:0
    left: unset;
  }
 
  .InputFromTo-to .DayPickerInput-Overlay {
    margin-left: 0px;
  }
`

export class CustomDatePicker extends Component {
 
  state = {
    from : new Date(),
    to : new Date()
  }

  showFromMonth = to => {
    const { from } = this.state
    if (!from) {
      return
    }
    this.to.getDayPicker().showMonth(from)
  };

  handleFromChange = from => {
    this.setState({from})
  };

  handleToChange = to => {
    this.setState({to})
    this.showFromMonth(to)
  };



  render() {
    const { from, to } = this.state
    const modifiers = { start: from, end: to }
    return (

        <InputFromTo>

    
          <span className='InputFromTo-From'>
            <DayPickerInput
              value={from}
              placeholder='วว/ดด/ปปปป'
              format='DD/MM/YYYY'
              formatDate={formatDate}
              parseDate={parseDate}
              dayPickerProps={{
                selectedDays: [from, { from, to }],
                disabledDays: { after: to },
                toMonth: to,
                modifiers,
                numberOfMonths: 2,
                onDayClick: () => this.to.getInput().focus()
              }}
              onDayChange={this.handleFromChange}
            />
          </span>
          {' '}
          -{' '}
          <span className='InputFromTo-to'>
            <DayPickerInput
              ref={el => (this.to = el)}
              value={to}
              placeholder='วว/ดด/ปปปป'
              format='DD/MM/YYYY'
              formatDate={formatDate}
              parseDate={parseDate}
              dayPickerProps={{
                selectedDays: [from, { from, to }],
                disabledDays: { before: from },
                modifiers,
                month: from,
                fromMonth: from,
                numberOfMonths: 2
              }}
              onDayChange={this.handleToChange}
            />
          </span>

          <span className='InputFromTo-From'>
           
           {this.state.from ? this.state.from.toDateString() : 'Invalid Date'}
          </span>
          {' '}
          -{' '}
          <span className='InputFromTo-to'>

          {this.state.to ? this.state.to.toDateString() : 'Invalid Date'}
          </span>
        </InputFromTo>
      
    )
  }
}

const customStyles = {
  container: provided => ({
    ...provided,
    padding: 0,
    border: 'none',
    color: '#4173ea',
    margin: 0,
    width: 180,
    cursor: 'pointer',
    marginRight: '16px'
  }),
  valueContainer: provided => ({
    ...provided,
    padding: 0,
    border: 'none',
    width: 180,
    color: '#4173ea'
  }),
  option: (provided, state) => ({
    ...provided,
    color: '#4173ea',
    backgroundColor: 'transparent',
    width: 180,
    cursor: 'pointer'
  }),
  control: (provided, state) => ({
    ...provided,
    borderColor: 'transparent',
    boxShadow: 'none',
    color: '#4173ea',
    backgroundColor: 'transparent',
    width: 180,
    ':hover': {
      borderColor: 'transparent',
      cursor: 'pointer'
    }
  }),
  placeholder: (provided, state) => ({
    ...provided,
    color: '#4173ea'

  }),
  singleValue: (provided, state) => ({
    ...provided,
    color: '#4173ea'

  })
}

export default CustomDatePicker
