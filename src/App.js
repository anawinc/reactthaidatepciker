import React from 'react';
import logo from './logo.svg';
import './App.css';
//import ThaiDatePicker from './ThaiDatePicker/es'
//import './ThaiDatePicker/es/datepicker.scss'

import './react-day-picker/lib/style.css';
import DayPickerInput from './react-day-picker/DayPickerInput'
import { formatDate, parseDate } from './react-day-picker/moment'
import CustomDatePicker from './CustomDatePicker'

function App() {

  return (
    <div className="App">
 
      <body>
        <div style={{  position: 'fixed', top: '45%', left: '30%'}}>
        {/* <DayPickerInput 
        keepFocus={true}
        dayPickerProps={{
            locale:'th'
        }}
            /> */}

         <CustomDatePicker />
        </div>
       </body>
    </div>
  );
}

export default App;
